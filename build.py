import subprocess
import argparse
import shutil
import os
import sys
from pathlib import Path
import json
from pprint import pprint
from urllib import request
import shutil

# define paths
root             = Path(os.getcwd())
firmware_path      = root / 'firmware'
src_path         = root / 'src'
www_path         = root / 'www'
lib_path         = src_path / 'lib'
micorwebsrv_path = lib_path / 'MicroWebSrv'

# pars command line arguments
parser = argparse.ArgumentParser(description='Build tool for ESP32-websocket-server.')
parser.add_argument('command', help='The command to execute.', choices=['setup', 'flash', 'install'])
parser.add_argument('-p','--port', help='The port the ESP32 is connected to.', required=True)
args = vars(parser.parse_args())

# extract data from command line arguments
command = args['command']
port = args['port']

# check if config file exitst
config_path = root / 'config.json'
if not config_path.is_file():
    print('No config file found. Exiting.')
    sys.exit()

# read the config json file
with open(config_path) as config_file:
    config = json.load(config_file)

# extract data from config file
firmware_url     = config['firmware_url']
firmware_filename = firmware_url.split('/')[-1]
microwebsrv_urls = config['microwebsrv_urls']

# downloads a file 
def download(url, file):
    print('Downloading ' + url)
    with request.urlopen(url) as response, open(file, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

# execute a rshell command
def execute_rshell_command(command):
    try:
        completed = subprocess.run(
            ['rshell',
            '--buffer-size=30',
            '--port=' + port,
            '-a',
            command],
            check=True,
            stdout=subprocess.PIPE )
        return completed.stdout.decode('utf-8')
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)

# clean up directory structure
def clean():
    try:
        shutil.rmtree(firmware_path)
        shutil.rmtree(micorwebsrv_path)
        print('Directories cleaned.')
    except FileNotFoundError:
        print('Skip cleaning directories.')

# recreate directory structure
def create_dirs():
    if not os.path.exists(firmware_path):
        os.makedirs(firmware_path)
        print('"firmware" directory created.')
    else:
        print('Skip creating "firmware" directory. Directory already exists.')

    if not os.path.exists(micorwebsrv_path):
        os.makedirs(micorwebsrv_path)
        print('"MicroWebSrv" directory created.')
    else:
        print('Skip creating "MicroWebSrv" directory.  Directory already exists.')
    open(micorwebsrv_path / '__init__.py', 'a').close()
    print('Empty __init__.py file created.')

def get_board_name():
    boards_output = execute_rshell_command('boards')
    relevant = boards_output.split('\r\n', 1)
    board_name = relevant[1].split(' ', 1)
    return board_name[0]

def execute_esptool_erase():
    try:
        subprocess.run(
            ['esptool',
            '--chip',
            'esp32',
            '--port',
            port,
            'erase_flash'],
            check=True)
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)

def execute_esptool_write():
    firmware = './firmware/' + str(firmware_filename)
    print(firmware)
    try:
        subprocess.run(
            ['esptool',
            '--chip',
            'esp32',
            '--port',
            port,
            'write_flash',
            '-z',
            '0x1000',
            firmware],
            check=True)
    except subprocess.CalledProcessError as err:
        print('ERROR:', err)

if command == 'setup':
    clean()
    create_dirs()
    download(firmware_url, firmware_path / firmware_filename)
    for url in microwebsrv_urls:
        filename = url.split('/')[-1]
        download(url, root / 'src' / 'lib' / 'MicroWebSrv' / filename)

if command == 'flash':
    print('Flashing ESP32:')
    input("Please hold down the BOOT button on your device. Press ENTER to continue.")
    execute_esptool_erase()
    input("Please hold down the BOOT button on your device. Press ENTER to continue.")
    execute_esptool_write()

if command == 'install':
    print('installing software on ESP32')
    board = get_board_name()
    source_files = src_path / '*'



