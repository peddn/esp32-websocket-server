from machine import Pin
import ujson
import uos

from lib.MicroWebSrv.microWebSrv import MicroWebSrv

led = Pin(2, Pin.OUT)

@MicroWebSrv.route('/control/led', 'POST')
def _control_led_post(httpClient, httpResp onse):   

	# Get the posted data as JSON object
	req_json = httpClient.ReadRequestContentAsJSON()

	# the channel and the command for this channel
	channel = None
	command = None

	# the response to the client
	response = {
		"channel": "led",
		"status": ""
	}

	# If there is a channel key in the json data
	if 'channel' in req_json:
		# extract the data
		channel = req_json['channel']
	
	# If there is a command key in the json data
	if 'command' in req_json:
		# extract the data
		command = req_json['command']

	# if the values are ok

	# turn on the internal LED
	if channel == 'led' and command == 'on':
		led.value(1)
		response['status'] = 'on'

	# turn off the internal LED
	if channel == 'led' and command == 'off':
		led.value(0)
		response['status'] = 'off'

	# send a state message back to the client
	response_json = ujson.dumps(response)
	httpResponse.WriteResponseJSONOk(obj=response_json, headers=None)

# starts the Webserver
def start():
	print(uos.getcwd())
	print('Starting restful Webserver.')
	srv = MicroWebSrv(webPath='www/')
	srv.Start(threaded=False)
