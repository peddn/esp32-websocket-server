# requirements
the following software must be available on your System
* Python 3.7
* git
* pipenv

# setup
clone the repo
```console
$ git clone https://github.com/peddn/esp32-websocket-server.git
```

install dependencies
```console
$ pipenv install
```

clone the MicrWebSrv repository
```console
$ mkdir vendor
$ cd vendor
$ git clone https://github.com/jczic/MicroWebSrv.git
```
download the latest build of micropython from
http://micropython.org/download#esp32
into the 'vendors' directory

# flash your ESP32 with micropython
download the latest build of micropython from
http://micropython.org/download#esp32 or use the version in the 'vendor' directory

* {port} - the port your microntroller is connected to
* {micropython.bin} - the micropython firmware you downloaded

```console
$ esptool --chip esp32 --port {port} erase_flash
$ esptool --chip esp32 --port {port} write_flash -z 0x1000 {micropython.bin}
```

# workflow
activate the virtual environment
```console
$ pipenv shell
```

activate rshell on COM3
```console
$ rshell --buffer-size=30 --port COM3 -a
```

useful rshell commands:
* help
* boards
* ls
* cp
* rm

list the connected boards
```console
$ boards
```

copy source files to the ESP32
```console
$ cp -r /src/* /pyboard
```

## activate repl
```console
$ repl
```

## reboot the ESP32

### programmatic
```console
>>> import machine
>>> machine.reset()
```

### hardware
press the 'EN' button

# test the application
use postman or similar software to send POST requests to the chip
https://www.getpostman.com/